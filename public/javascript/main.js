//=======================================================================]'
console.log('js loaded')
// landing page
let section = document.querySelector('main section:first-of-type');
// content
let cta1 = document.querySelector('.cta-1');
let cta2 = document.querySelector('.cta-2');
let cta3 = document.querySelector('.cta-3');
// pagination
let cta1Marker = document.getElementById('cta-1');
let cta2Marker = document.getElementById('cta-2');
let cta3Marker = document.getElementById('cta-3');
console.log(section.className)
setInterval(myMethod, 7000);

function myMethod() {
    switch (section.className) {
        case 'slide1':
            console.log('slide1')
            section.classList.remove('slide1');
            section.classList.add('slide2');
            cta2.classList.remove('hidden');
            cta1.classList.add('hidden');
            cta1Marker.classList.remove('active');
            cta2Marker.classList.add('active');
            break;
        case 'slide2':
            section.classList.remove('slide2');
            section.classList.add('slide3');
            cta3.classList.remove('hidden');
            cta2.classList.add('hidden');
            cta2Marker.classList.remove('active');
            cta3Marker.classList.add('active');
            break;
        case 'slide3':
            section.classList.remove('slide3');
            section.classList.add('slide1');
            cta1.classList.remove('hidden');
            cta3.classList.add('hidden');
            cta3Marker.classList.remove('active');
            cta1Marker.classList.add('active');
            break;
    }
}
// landing page controls

cta1Marker.addEventListener('click', function () {
    console.log('cta1');
    switch (section.className) {
        case 'slide2':
            section.classList.remove('slide2');
            section.classList.add('slide1');
            cta1.classList.remove('hidden');
            cta2.classList.add('hidden');
            cta2Marker.classList.remove('active');
            cta1Marker.classList.add('active');
            break;
        case 'slide3':
            section.classList.remove('slide3');
            section.classList.add('slide1');
            cta1.classList.remove('hidden');
            cta3.classList.add('hidden');
            cta3Marker.classList.remove('active');
            cta1Marker.classList.add('active');
            break;
    }
});
cta2Marker.addEventListener('click', function () {
    console.log('cta2');
    switch (section.className) {
        case 'slide1':
            section.classList.remove('slide1');
            section.classList.add('slide2');
            cta2.classList.remove('hidden');
            cta1.classList.add('hidden');
            cta1Marker.classList.remove('active');
            cta2Marker.classList.add('active');
            break;
        case 'slide3':
            section.classList.remove('slide3');
            section.classList.add('slide2');
            cta2.classList.remove('hidden');
            cta3.classList.add('hidden');
            cta3Marker.classList.remove('active');
            cta2Marker.classList.add('active');
            break;
    }
});
cta3Marker.addEventListener('click', function () {
    console.log('cta3');
    switch (section.className) {
        case 'slide1':
            section.classList.remove('slide1');
            section.classList.add('slide3');
            cta3.classList.remove('hidden');
            cta1.classList.add('hidden');
            cta1Marker.classList.remove('active');
            cta3Marker.classList.add('active');
            break;
        case 'slide2':
            section.classList.remove('slide2');
            section.classList.add('slide3');
            cta3.classList.remove('hidden');
            cta2.classList.add('hidden');
            cta2Marker.classList.remove('active');
            cta3Marker.classList.add('active');
            break;
    }
});
// ======================================================================
