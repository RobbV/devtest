const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Order Schema
const OrderSchema = new Schema({
    id: {
        type: 'Number'
    },
    note: {
        type: 'Mixed'
    },
    email: {
        type: 'Mixed'
    },
    taxes_included: {
        type: 'Boolean'
    },
    currency: {
        type: 'String'
    },
    subtotal_price: {
        type: 'String'
    },
    total_tax: {
        type: 'String'
    },
    total_price: {
        type: 'String'
    },
    invoice_sent_at: {
        type: 'Mixed'
    },
    created_at: {
        type: 'Date'
    },
    updated_at: {
        type: 'Date'
    },
    tax_exempt: {
        type: 'Boolean'
    },
    completed_at: {
        type: 'Mixed'
    },
    name: {
        type: 'String'
    },
    status: {
        type: 'String'
    },
    line_items: {
        type: [
            'Mixed'
        ]
    },
    shipping_address: {
        type: 'Mixed'
    },
    billing_address: {
        type: 'Mixed'
    },
    invoice_url: {
        type: 'String'
    },
    applied_discount: {
        type: 'Mixed'
    },
    order_id: {
        type: 'Mixed'
    },
    shipping_line: {
        type: 'Mixed'
    },
    tax_lines: {
        type: [
            'Mixed'
        ]
    },
    tags: {
        type: 'String'
    },
    note_attributes: {
        type: 'Array'
    },
    admin_graphql_api_id: {
        type: 'String'
    },
    customer: {
        type: 'Mixed'
    }
});

// export mongoose schema
module.exports = Orders = mongoose.model('order', OrderSchema);