const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Product Schema
const ProductSchema = new Schema({
  id: {
    type: 'Number'
  },
  title: {
    type: 'String'
  },
  body_html: {
    type: 'String'
  },
  vendor: {
    type: 'String'
  },
  product_type: {
    type: 'String'
  },
  created_at: {
    type: 'Date'
  },
  handle: {
    type: 'String'
  },
  updated_at: {
    type: 'Date'
  },
  published_at: {
    type: 'Date'
  },
  template_suffix: {
    type: 'Mixed'
  },
  tags: {
    type: 'String'
  },
  published_scope: {
    type: 'String'
  },
  admin_graphql_api_id: {
    type: 'String'
  },
  variants: {
    type: ['Mixed']
  },
  options: {
    type: ['Mixed']
  },
  images: {
    type: ['Mixed']
  },
  image: {
    id: {
      type: 'Number'
    },
    product_id: {
      type: 'Number'
    },
    position: {
      type: 'Number'
    },
    created_at: {
      type: 'Date'
    },
    updated_at: {
      type: 'Date'
    },
    alt: {
      type: 'Mixed'
    },
    width: {
      type: 'Number'
    },
    height: {
      type: 'Number'
    },
    src: {
      type: 'String'
    },
    variant_ids: {
      type: 'Array'
    },
    admin_graphql_api_id: {
      type: 'String'
    }
  }
});

// export mongoose schema
module.exports = Product = mongoose.model('product', ProductSchema);
