// Import required packages
const dotenv = require('dotenv').config();
const https = require('https');
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const Shopify = require('shopify-api-node');
const mongoose = require('mongoose');
const app = express();

// setup view engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

//DB config
const db = require('./config/keys').mogoURI;

// Connect to mongoDB
mongoose
  .connect(db)
  .then(() => console.log('Connect to MongoDB'))
  .catch(err => console.log(err));

// Load models
const Product = require('./models/Products');
const Order = require('./models/Orders');

// Shopify API credentials
const apiKey = process.env.SHOPIFY_API_KEY;
const apiPassword = process.env.SHOPIFY_API_PASSWORD;
const apiSecret = process.env.SHOPIFY_API_SECRET;

// access the shopify api
const shopify = new Shopify({
  shopName: 'gointegrations-devtest',
  apiKey: apiKey,
  password: apiPassword
});
//=========================================================================
// @route    GET /
// @desc     GET Use the shopify api to save the product list to mongodb
app.get('/save-products', (req, res) => {
  //access the api to grab the list products
  shopify.product
    .list({ limit: 3 })
    .then(products => {
      // loop through the JSON array and save each product to mongoDB
      for (let i = 0; i < products.length; i++) {
        //check the db for the product ID to avoid duplicate products
        Product.findOne({ id: products[i].id }).then(product => {
          if (product) {
            console.log('Product ' + products[i].id + ' already exists');
          } else {
            console.log(i);
            const newProduct = new Product(products[i]);
            // save to mongodb
            newProduct
              .save()
              .then(product => console.log('Saved product' + i))
              .catch(err => console.log(err));
          }
        });
      }
      res.send(products);
    })
    .catch(err => console.log(err));
});
//=========================================================================
// @route    GET /
// @desc     GET the Landing Page
// @access   Public
app.get('/', (req, res) => {
  // get all the products from the DB
  Product.find((err, products) => {
    if (err) {
      console.log(err);
    } else {
      res.render('index', {
        title: "DevTest",
        products: products,
        orderID: null
      });
    }
  });
});
//=========================================================================
// @route    POST /
// @desc     POST find the order in the DB and show user the order 
// @access   Public
app.post('/', (req, res) => {
  // grab the id from the url
  let _id = req.body.orderNumber;
  // find the order in the DB
  Order.findOne({ id: _id }, (err, order) => {
    if (err) {
      console.log(err)
    } else if (order == null) {
      res.redirect('/');
    } else {
      console.log(order);
      res.render('order', {
        title: "DevTest Order",
        order: order,
        orderID: _id
      })
    }

  })
});
//=========================================================================
// @route    GET /
// @desc     GET the product id from the url and send a draft order to the shopify api and save the response to the DB
// @access   Public
app.get('/order/:_id', (req, res) => {
  // grab the id from the url
  let _id = req.params._id;

  // find the product in mongodb 
  Product.findById(_id, (err, product) => {
    if (err) {
      console.log(err);
    } else {
      console.log(product.variants[0].price * 0.75);
      shopify.draftOrder.create({
        "line_items": [
          {
            "title": product.title,
            "price": product.variants[0].price,
            "quantity": 1,
            "applied_discount": {
              "description": "GoIntegration Discount",
              "value_type": "percentage",
              "value": "25",
              "amount": product.variants[0].price * 0.25,
              "title": "Razor"
            }
          }
        ]
      })
        .then(draftOrder => {
          // save the order to mongodb\
          console.log(draftOrder);
          const newOrder = new Order(draftOrder);
          //save new order
          newOrder
            .save()
            .then(newOrder => {
              console.log(newOrder.id)
              // get all the products from the DB
              Product.find((err, products) => {
                if (err) {
                  console.log(err);
                } else {
                  res.redirect('/');
                  console.log(newOrder.id)
                }
              });
            })
            .catch(err => console.log(err))
        })
        .catch(err => console.log(err));
    }
  });
});
//=========================================================================
// @route    GET /
// @desc     GET the order number and delete the order from mongoDB
// @access   Public
app.get('/order/delete/:_id', (req, res) => {
  let id = req.params._id;
  console.log(id);
  Order.remove({ id: id }, (err) => {
    if (err) {
      console.log(err);
    } else {
      res.redirect('/')
    }
  })
});
//=========================================================================
app.listen(process.env.PORT || 5000, () => {
  console.log('Application Running');
});
